#include <exception>
#include <cstdlib>
#include <vector>

int foo()
{
throw std::exception();
return 777;
}

void bar ()
{
//    std::vector<int> vec;
    foo();
    foo();
}

int bazz()
{
	return 0;
}

int main()
{
    try {
        bar();
//        foo();
//        bazz();
    }
    catch (std::exception &e){}

    return 0;
}
