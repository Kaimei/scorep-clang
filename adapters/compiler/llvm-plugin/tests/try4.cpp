//
// Created by sdoebel on 8/3/17.
//

#include <cstdio>
#include <cstdlib>
#include <exception>
#include "Bla.h"
#include "Bla2.h"

int foo()
{
    throw  std::exception();
}

void bar()
{
    Bla* bla = new Bla(20);
    Bla2* bla2 = new Bla2(39);
    int i = bla->blaFoo();
    foo();
//    foo();
}

int bazz()
{
    return 0;
}

int main()
{
    return 0;
}
