#include <exception>

/** noinline prevents from being inlined into main */

int __attribute__((noinline)) a() {
    throw (std::exception());
    return 0;
}

int __attribute__((noinline)) b() {
    try {
        return a();
    } catch (...) {}

    return 1;
}

int main() {
    return b();
}