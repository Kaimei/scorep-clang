#include <exception>
#include <stdio.h>

/**
 * check if indirect calls are also closed
 */

/** noinline prevents from being inlined into main */
int __attribute__((noinline)) a() {
//    throw (std::exception());
    throw (1.0);
    return 0;
}

int __attribute__((noinline)) b() {
    return a();
}

int __attribute__((noinline)) c() {
    try {
        return b();
    } catch (int e) {
        printf("this print should not be executed on clean up \n");
    }

    return 1;
}

int main() {
    try {
        return c();
    }
    catch (...) {
        return 0;
    }
}
