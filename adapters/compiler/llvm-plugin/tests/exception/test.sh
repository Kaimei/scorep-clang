/home/sdoebel/Scorep/local/TRY_TUD_llvm/bin/scorep-clang++ basicTryCatch.cpp scorep_llvm_callback_functions.o > /dev/null
echo "basic test"
./a.out
echo ""
echo ""

/home/sdoebel/Scorep/local/TRY_TUD_llvm/bin/scorep-clang++ basicTryCatch3Functions.cpp scorep_llvm_callback_functions.o > /dev/null
echo "3 functions"
./a.out
echo ""
echo ""

/home/sdoebel/Scorep/local/TRY_TUD_llvm/bin/scorep-clang++ -std=c++11 object.cpp scorep_llvm_callback_functions.o > /dev/null
echo "test with object cleanup"
./a.out
echo ""
echo ""

/home/sdoebel/Scorep/local/TRY_TUD_llvm/bin/scorep-clang++ -std=c++11 twoObjects.cpp scorep_llvm_callback_functions.o > /dev/null
echo "test with 2 objects"
./a.out
echo ""
echo ""

/home/sdoebel/Scorep/local/TRY_TUD_llvm/bin/scorep-clang++ -std=c++11 threeObjects.cpp scorep_llvm_callback_functions.o > /dev/null
echo "test with 3 objects"
./a.out
echo ""
echo ""

/home/sdoebel/Scorep/local/TRY_TUD_llvm/bin/scorep-clang++ -std=c++11 noCatchCase.cpp scorep_llvm_callback_functions.o > /dev/null
echo "no fitting catch case in b"
./a.out
echo ""
echo ""