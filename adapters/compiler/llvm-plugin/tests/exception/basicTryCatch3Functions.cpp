#include <exception>

/**
 * check if indirect calls are also closed
 */

/** noinline prevents from being inlined into main */
int __attribute__((noinline)) a() {
    throw (std::exception());
    return 0;
}

int __attribute__((noinline)) b() {
    return a();
}

int __attribute__((noinline)) c() {
    try {
        return b();
    } catch (...) {}

    return 1;
}

int main() {
    return c();
}