#include <exception>
#include <stdio.h>

class Bla {
public:
    Bla() = default;

    Bla(int i) {
    }

    int __attribute__((noinline)) foo() {
        return 0;
    }

    int __attribute__((noinline)) throwException() {
        throw std::exception();
        return 99;
    }

    ~Bla() noexcept(true) {
        printf("Destructor was called\n");
    }
};