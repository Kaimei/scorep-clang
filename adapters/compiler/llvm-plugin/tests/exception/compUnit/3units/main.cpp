#include <exception>

extern "C" int call_from_middle();

int main()
{
    int i = 0;
    try {
        i = call_from_middle();
    } catch (...) {}

    return i;
}