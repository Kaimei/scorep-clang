#include <exception>

static int __attribute__((noinline)) throw_from_lib() {
    throw std::exception();
    return 1;
}


extern "C" int call_from_lib() {
    return throw_from_lib();
}


