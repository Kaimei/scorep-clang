#include <exception>
#include "Bla.h"

int __attribute__((noinline)) a() {
    Bla obj = Bla();
    obj.foo();
    Bla obj2 = Bla();
    return obj.throwException();
}


int __attribute__((noinline)) b() {
    return a();
}


int __attribute__((noinline)) c() {
    try {
        return b();
    } catch (...) {}

    return 0;
}


int __attribute__((noinline)) main() {
    return c();
}