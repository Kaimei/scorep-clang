#include <exception>
#include <cstdlib>

int foo()
{
throw std::exception();
return 777;
}

void bar ()
{
    foo();
}

int bazz()
{
	return 0;
}

int main()
{
    try {
        bar();
//        foo();
//        bazz();
    }
    catch (std::exception &e){}

    return 0;
}
