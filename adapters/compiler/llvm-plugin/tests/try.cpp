#include <exception>
#include <cstdlib>
#include <cstdio>

int bar()
{
    return 3;
}
int foo()
{
    int i;
    int j[3];
    try {
        i = 0;
        i = bar();
        scanf("%d", &i);
        i = j[i];
    } catch (std::exception e){i = 666;}
    catch (...){i = 667;}
    
    
    try {
        j[0] = 0;
        j[0] = bar();
        scanf("%d", &j[0]);
        i = j[i];
    } catch (std::exception e){j[2] = 666;}
    catch (...){j[2] = 667;}
    
    return i+j[2]+j[0];
}

int main()
{
    return 0;
}
