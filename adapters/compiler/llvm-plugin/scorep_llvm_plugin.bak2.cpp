/*
 * Dummy plug-in that prints a message for each function.
 * Inspired by Adrian Sampson:
 *
 * https://github.com/sampsyo/llvm-pass-skeleton
 *
 * Will be replaced by a functional instrumentation plug-in
 */

/**
 * @file
 *
 * @brief  A dummy plug-in to demonstrate the principle of
 *         a LLVM plug-in. This plug-in is executed at
 *         compile-time and prints out the name of every
 *         function.
 *
 */
#include <llvm/Pass.h>
#include <llvm/IR/Function.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/IR/LegacyPassManager.h>
#include <llvm/IR/InstrTypes.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/BasicBlock.h>
#include <llvm/Transforms/Utils/BasicBlockUtils.h>
#include <llvm/IR/Module.h>


using namespace llvm;

namespace
{
struct SkeletonPass : public FunctionPass
{
    static char ID;

    SkeletonPass() : FunctionPass( ID )
    {
    }

    virtual bool
    runOnFunction( Function& F )
    {
        Module*      M   = F.getParent();
        LLVMContext& Ctx = F.getContext();
        IRBuilder<>  builder( Ctx );

        // helper pointer types
        auto* i32PtrTy = PointerType::get( Type::getInt32Ty( Ctx ), 0 );
        auto* i8PtrTy  = PointerType::get( Type::getInt8PtrTy( Ctx ), 0 );

        GlobalVariable* scrorepRegionHandle = new GlobalVariable( *M, Type::getInt32Ty( M->getContext() ), false,
                                                                  GlobalValue::LinkageTypes::InternalLinkage,
                                                                  ConstantInt::get( M->getContext(),
                                                                                    APInt( 32, 0, 10 ) ),
                                                                  F.getName() + ".scorep_region_handle" );



        if (scorepRegionDescriptor == nullptr)
        {
            createStructType(M);
            createFunctionPrototypes(M);
        }

        auto* sPtrTy = PointerType::get( scorepRegionDescriptor, 0 ); // pointer type of scorepRegionDescriptor

        auto* scorepRegionDescVar = new GlobalVariable( *M, scorepRegionDescriptor, false,
                                                        GlobalValue::LinkageTypes::ExternalLinkage,
                                                        ConstantPointerNull::get( sPtrTy ),
                                                        F.getName() + ".scorep_region_descr", nullptr );
        scorepRegionDescVar->setAlignment( 8 );

        /** initialize static chars */
        auto  nameText = ConstantDataArray::getString( Ctx, F.getName() );
        auto* name     = new GlobalVariable( *M, nameText->getType(), true,
                                             GlobalValue::LinkageTypes::PrivateLinkage, nameText, F.getName() + ".name" );
        name->setAlignment( 1 );

        auto  canonicalNameText = ConstantDataArray::getString( Ctx, "canonical name" );
        auto* canonicalName     = new GlobalVariable( *M, canonicalNameText->getType(), true,
                                                      GlobalValue::LinkageTypes::PrivateLinkage, canonicalNameText,
                                                      F.getName() + ".canonical_name" );
        canonicalName->setAlignment( 1 );

        auto  fileText = ConstantDataArray::getString( Ctx, "Some pretty filename" );
        auto* file     = new GlobalVariable( *M, fileText->getType(), true,
                                             GlobalValue::LinkageTypes::PrivateLinkage, fileText, F.getName() + ".file" );
        file->setAlignment( 1 );

        /** basic block handling */
        // jump to last instruction of the first basic block
        auto         bbIter   = F.begin();
        BasicBlock&  firstBb  = *bbIter;
        auto         instIter = firstBb.rbegin();//++(bb.rbegin()); // second last
        Instruction& inst     = *instIter;
        builder.SetInsertPoint( &inst );

        // initialize region descriptor
        Value* gep = builder.CreateInBoundsGEP( scorepRegionDescVar, { builder.getInt32( 0 ), builder.getInt32( 0 ) } );
        builder.CreateStore( ConstantExpr::getBitCast( scrorepRegionHandle, i32PtrTy ), gep );


        gep = builder.CreateInBoundsGEP( scorepRegionDescVar, { builder.getInt32( 0 ), builder.getInt32( 1 ) } );
        builder.CreateStore( ConstantExpr::getBitCast( name, i8PtrTy ), gep );
        gep = builder.CreateInBoundsGEP( scorepRegionDescVar, { builder.getInt32( 0 ), builder.getInt32( 2 ) } );
        builder.CreateStore( ConstantExpr::getBitCast( canonicalName, i8PtrTy ), gep );
        gep = builder.CreateInBoundsGEP( scorepRegionDescVar, { builder.getInt32( 0 ), builder.getInt32( 3 ) } );
        builder.CreateStore( ConstantExpr::getBitCast( file, i8PtrTy ), gep );


        // insert Scorep register region
        auto            load                 = builder.CreateLoad( scrorepRegionHandle );
        Value*          icmp                 = builder.CreateICmpEQ( load, ConstantInt::get( Ctx, APInt( 32, "0", 10 ) ) );
        TerminatorInst* registerPluginBranch = SplitBlockAndInsertIfThen( icmp, &inst, false );
        builder.SetInsertPoint( registerPluginBranch );
        builder.CreateCall( scorep_plugin_register_region_Func, scorepRegionDescVar );

        // insert Scorep enter region
        builder.SetInsertPoint( &inst );
        load = builder.CreateLoad( scrorepRegionHandle );
        icmp = builder.CreateICmpNE( load, ConstantInt::get( Ctx, APInt( 32, "-1", 10 ) ) );
        TerminatorInst* enterPluginBranch = SplitBlockAndInsertIfThen( icmp, &inst, false );
        builder.SetInsertPoint( enterPluginBranch );
        builder.CreateCall( scorep_plugin_enter_region_Func, load );


        // jump to last instruction of the last basic block
        bbIter = --( F.end() );
        BasicBlock& lastBb = *bbIter;
        instIter = lastBb.rbegin();
        Instruction& lastInst = *instIter;

        // insert Scorep exit region
        builder.SetInsertPoint( &lastInst );
        load = builder.CreateLoad(
            scrorepRegionHandle );
        icmp = builder.CreateICmpNE( load, ConstantInt::get( Ctx, APInt( 32, "-1", 10 ) ) );
        TerminatorInst* exitPluginBranch = SplitBlockAndInsertIfThen( icmp, &lastInst, false );
        builder.SetInsertPoint( exitPluginBranch );
        builder.CreateCall( scorep_plugin_exit_region_Func, load );

        return true;
    }

    void createStructType(Module* M)
    {
        LLVMContext &Ctx = M->getContext();

        auto *i32Ty = Type::getInt32Ty(Ctx);
        auto *i32PtrTy = PointerType::get(i32Ty, 0);
        auto *i8PtrTy = PointerType::get(Type::getInt8PtrTy(Ctx), 0);

        scorepRegionDescriptor = StructType::create(Ctx,
                                                    "struct.scorep_region_description");//
        std::vector<llvm::Type *> params;
        params.push_back(i32PtrTy);               // handle ref
        params.push_back(i8PtrTy);                // name
        params.push_back(i8PtrTy);                // cononical name
        params.push_back(i8PtrTy);                // file
        params.push_back(i32Ty);                  // begin lno
        params.push_back(i32Ty);                  // end lno
        params.push_back(i32Ty);                  // flags
        scorepRegionDescriptor->setBody(params);  // ArrayRef<llvm::Type *>(params)
    }

    void createFunctionPrototypes(Module *M)
    {
        LLVMContext &Ctx = M->getContext();
        Type *voidTy = Type::getVoidTy(Ctx);
        Type *i32Ty = Type::getInt32Ty(Ctx);

        auto *sPtrTy = PointerType::get(scorepRegionDescriptor, 0);

        /** register linked ScoreP functions */
        scorep_plugin_register_region_Func = M->getOrInsertFunction(
                "scorep_plugin_register_region", voidTy, sPtrTy, NULL
        );


        scorep_plugin_exit_region_Func = M->getOrInsertFunction(
                "scorep_plugin_exit_region", voidTy, i32Ty, NULL
        );

        scorep_plugin_enter_region_Func = M->getOrInsertFunction(
                "scorep_plugin_enter_region", voidTy, i32Ty, NULL
        );
    }

protected:
    Constant *scorep_plugin_register_region_Func;
    Constant *scorep_plugin_exit_region_Func;
    Constant *scorep_plugin_enter_region_Func;

    StructType *scorepRegionDescriptor = nullptr;
};
}

char SkeletonPass::ID = 0;

// Automatically enable the pass.
// http://adriansampson.net/blog/clangpass.html
static void
registerSkeletonPass( const PassManagerBuilder&,
                      legacy::PassManagerBase& PM )
{
    PM.add( new SkeletonPass() );
}

static RegisterStandardPasses
    RegisterMyPass( PassManagerBuilder::EP_EarlyAsPossible,
                    registerSkeletonPass );
