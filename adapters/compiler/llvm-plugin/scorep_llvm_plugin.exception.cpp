/**
 * @file
 *
 * @brief  A plugin to instrument any function for usage with Score-P at compile-time.
 *
 */

#include <tuple>

#include <llvm/Pass.h>
#include <llvm/IR/Function.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/IR/LegacyPassManager.h>
#include <llvm/IR/InstrTypes.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/BasicBlock.h>
#include <llvm/Transforms/Utils/BasicBlockUtils.h>
#include <llvm/IR/Module.h>
#include <llvm/Transforms/Utils/Local.h>     // changeToInvokeAndSplitBasicBlock https://llvm.org/doxygen/Local_8cpp_source.html#l01441

#include <llvm/Analysis/EHPersonalities.h>

#include <cxxabi.h>

#include <llvm/IR/DebugInfo.h>

using namespace llvm;

namespace
{
struct ScorepPass : public FunctionPass
{
    static char ID;

    ScorepPass() : FunctionPass( ID )
    {
    }

    virtual StringRef
    getPassName() const override
    {
        return "Scorep instrumentation";
    }

    virtual bool
    runOnFunction( Function& F ) override
    {
        outs() << "I saw a function called " << F.getName() << "!\n";
        std::string ignore_reason;

        if ( !is_instrumentable( F, &ignore_reason ) )
        {
            outs() << "Ignore: " + F.getName().str() + " reason: " + ignore_reason + "\n";
            /* function is not modified */
            return false;
        }

        Module*      m   = F.getParent();
        LLVMContext& ctx = F.getContext();
        IRBuilder<>  builder( ctx );


        /* create types and function prototypes only once */
        if ( region_description_type == nullptr )
        {
            region_description_type = create_struct_type( m );
            std::tie( scorep_plugin_register_region_func,
                      scorep_plugin_enter_region_func,
                      scorep_plugin_exit_region_func,
                      personality_function
            ) = create_function_prototypes( m );

            // just a dummy function for debugging
            // TODO remove
            scorep_plugin_dummy_func = F.getParent()->getOrInsertFunction( "scorep_plugin_dummy", Type::getVoidTy( F.getParent()->getContext() ), Type::getInt32Ty( F.getContext() ), NULL );

            exception_type = { builder.getInt8PtrTy(), builder.getInt32Ty() };
        }


        auto         parsed_function_tuple = parse_function( F );
        auto&        exit_points           = std::get<0>( parsed_function_tuple );
        auto&        call_inst             = std::get<1>( parsed_function_tuple );
        auto&        resume_block          = std::get<2>( parsed_function_tuple );
        Instruction& start_point           = get_insert_begin( F );

        create_global_vars( F );
        insert_scorep_function_calls( F, builder, start_point, exit_points );

        expection_handling( F, call_inst, resume_block, builder );

        return true;
    }

private:

    /**
     * Check if a function can be instrumented
     * @param f function to check
     * @return true, if function can be instrumented
     */
    bool
    is_instrumentable( const Function& f, std::string* error = nullptr )
    {
        if ( has_no_instrument_attribute( f, error )
             || has_empty_body( f, error )
             || is_openmp_function( f, error )
             || is_artificial( f, error )
             || is_exception( f, error )
             || is_scorep_filtered( f, error ) )
        {
            return false;
        }

        return true;
    }

    /**
     * Check if function has an empty body
     * @param f function to check
     * @param error pointer to error message
     * @return true, if function has an empty body
     * @todo use built-in functions?
     */
    bool
    has_empty_body( const Function& f, std::string* error = nullptr )
    {
        unsigned long size = f.getEntryBlock().getInstList().size();

        if ( f.getReturnType() == Type::getVoidTy( f.getContext() ) && size == 1 )
        {
            if ( error != nullptr )
            {
                *error = "is empty";
            }
            return true;
        }
        return false;
    }

    /** Check if functions name starts with '.omp_outlined.
     * @param f function to check
     * @param error pointer to error message
     * @return true, if functions name starts with '.omp_outlined.'
     */
    bool
    is_openmp_function( const Function& f, std::string* error = nullptr )
    {
        std::string omp_outlined_func_name( ".omp_outlined." );
        std::string func_name_as_string = f.getName().str();
        if ( !func_name_as_string.compare( 0, omp_outlined_func_name.size(), omp_outlined_func_name ) )
        {
            if ( error != nullptr )
            {
                *error = "is OMP outlined";
            }
            return true;
        }
        return false;
    }

    /**
     * Check if function has attribute "scorep_no_instrument"
     * @param f function to check
     * @param error pointer to error message
     * @return true, if it has attribute "scorep_no_instrument"
     * @todo is ignored?
     */
    bool
    has_no_instrument_attribute( const Function& f, std::string* error = nullptr )
    {
        if ( f.hasFnAttribute( "scorep_no_instrument" ) )
        {
            if ( error != nullptr )
            {
                *error = "has attribute scorep_no_instrument";
            }
            return true;
        }
        return false;
    }

    /**
     * Check if the function is created artificially by the compiler
     * @param f function to check
     * @param error pointer to error message
     * @return true, if function is artificial
     * @todo better characteristic, maybe we should filter the filename
     */
    bool
    is_artificial( const Function& f, std::string* error = nullptr )
    {
        std::string cxx_name            = "__cxx_";
        std::string glob_name           = "_GLOBAL__";
        std::string func_name_as_string = f.getName().str();

        if ( func_name_as_string.find( cxx_name ) != std::string::npos
             || func_name_as_string.find( glob_name ) != std::string::npos )
        {
            if ( error != nullptr )
            {
                *error = "is artificial";
            }
            return true;
        }
        return false;
    }

    /**
     * Check if the function is an exception function
     * @param f function to check
     * @param error pointer to error message
     * @return true, if function is an exception
     * @todo better characteristic
     */
    bool
    is_exception( const Function& f, std::string* error = nullptr )
    {
        std::string exception_name      = "exception";
        std::string func_name_as_string = f.getName().str();

        if ( func_name_as_string.find( exception_name ) != std::string::npos )
        {
            if ( error != nullptr )
            {
                *error = "is exception";
            }
            return true;
        }
        return false;
    }

    /**
     * Check if the function is filtered by ScoreP file
     * @param f function to check
     * @param error pointer to error message
     * @return true, if function is filtered
     * @todo implementation
     */
    bool
    is_scorep_filtered( const Function& f, std::string* error = nullptr )
    {
        return false;
    }

    /**
     * Create the struct type for region_descriptor and insert it to LLVM IR.
     *
     * @param m     module in which context struct will be inserted
     *
     * @warning do not change order and insert new elements only ar the end
     */
    StructType*
    create_struct_type( Module* m )
    {
        LLVMContext& ctx = m->getContext();

        auto* i32_type     = Type::getInt32Ty( ctx );
        auto* i32_ptr_type = PointerType::get( i32_type, 0 );
        //auto *i8_ptr_type = PointerType::get(Type::getInt8PtrTy(ctx), 0);
        auto* i8_ptr_type = PointerType::getInt8PtrTy( ctx );


        StructType*              descriptor = StructType::create( ctx, "struct.scorep_region_description" );
        std::vector<llvm::Type*> params;
        params.push_back( i32_ptr_type );               // handle ref
        params.push_back( i8_ptr_type );                // name
        params.push_back( i8_ptr_type );                // canonical name
        params.push_back( i8_ptr_type );                // file
        params.push_back( i32_type );                   // begin lno
        params.push_back( i32_type );                   // end lno
        params.push_back( i32_type );                   // flags

        descriptor->setBody( params );

        return descriptor;
    }

    /**
     * Get pretty name from canonical.
     * @param s     Mangled region name.
     * @return
     * @todo implement
     */
    std::string
    demangle( const std::string& str )
    {
        char* demangled = abi::__cxa_demangle( str.c_str(), NULL, NULL, NULL );

        std::string res = str;
        if ( demangled != nullptr )
        {
            res = std::string( demangled );
        }
        else
        {
            res = str;
        }

        ::free( demangled );
        return res;
    }

    /**
     * Create prototypes for Score-P's register, enter and exit function and insert them to LLVM IR.
     *
     * @param m     module in which context prototypes will be inserted
     * @return tuple of function prototypes (register, enter, exit)
     */
    std::tuple<Constant*, Constant*, Constant*, Constant*>
    create_function_prototypes( Module* m )
    {
        LLVMContext& ctx       = m->getContext();
        Type*        void_type = Type::getVoidTy( ctx );
        Type*        i32_type  = Type::getInt32Ty( ctx );

        PointerType* struct_ptr_type = PointerType::get( region_description_type, 0 );

        /** register linked Score-P functions */
        Constant* register_func = m->getOrInsertFunction( "scorep_plugin_register_region",
                                                          void_type,
                                                          struct_ptr_type,
                                                          NULL );
        Constant* enter_func = m->getOrInsertFunction( "scorep_plugin_enter_region",
                                                       void_type,
                                                       i32_type,
                                                       NULL );
        Constant* exit_func = m->getOrInsertFunction( "scorep_plugin_exit_region",
                                                      void_type,
                                                      i32_type,
                                                      NULL );

        Constant* pers_func = m->getOrInsertFunction( getEHPersonalityName( EHPersonality::GNU_C ),
                                                             i32_type,
                                                             /* personality parameter */
//                                                             i32_type,
//                                                             i32_type,
//                                                             Type::getInt64Ty( m->getContext() ),
//                                                             Type::getInt8PtrTy( m->getContext() ),
//                                                             Type::getInt8PtrTy( m->getContext() ),
                                                             NULL );

        return std::make_tuple( register_func, enter_func, exit_func, pers_func );
    }

    /**
     * Create a global string variable
     * @param f
     * @param value
     * @param name variable's name you see in IR
     * @return
     */
    GlobalVariable*
    create_global_string( Function& f, const std::string& value, const std::string& name = "" )
    {
        auto        string   = ConstantDataArray::getString( f.getContext(), value );
        std::string var_name = "";
        var_name = f.getName().str() + "." + name;

        GlobalVariable* global = new GlobalVariable( *f.getParent(),
                                                     string->getType(),
                                                     true,
                                                     GlobalValue::LinkageTypes::PrivateLinkage,
                                                     string,
                                                     var_name
                                                     );
        global->setAlignment( 1 );
        return global;
    }



    /**
     * get meta data
     * @param f
     * @return tuple of (begin lno, end lno, filename)
     */
    std::tuple<uint32_t, uint32_t, std::string>
    get_meta_data( Function& f )
    {
        uint32_t     begin_lno       = 0;
        uint32_t     end_lno         = 0;
        std::string  file            = "";
        Instruction& end_instruction = get_insert_end( f );

        /* get begin line number and filename from functions node */
        if ( auto sub = f.getSubprogram() )
        {
            begin_lno = sub->getLine();
            file      = sub->getFilename().str();
        }

        /* end line number is not included in functions meta data, therefore try to get line number from the
           last instruction (return) which is function's end */
        if ( DILocation* Loc = end_instruction.getDebugLoc() )
        {
            end_lno = Loc->getLine();
        }

        return std::make_tuple( begin_lno, end_lno, file );
    }



    /**
     * Creates handler for region and descriptor and add them to LLVM IR.
     *
     * @param f
     * @param builder
     */
    void
    create_global_vars( Function& f )
    {
        Module*   m            = f.getParent();
        StringRef func_name    = f.getName();
        Type*     i32_ptr_type = Type::getInt32PtrTy( m->getContext() );
        Type*     i32_type     = Type::getInt32Ty( m->getContext() );
        Type*     i8_ptr_type  = Type::getInt8PtrTy( m->getContext() );

        auto metadata = get_meta_data( f );

        region_handle = new GlobalVariable( *m,
                                            Type::getInt32Ty( m->getContext() ),
                                            false,
                                            GlobalValue::LinkageTypes::InternalLinkage,
                                            ConstantInt::get( i32_type, 0 ),
                                            func_name + ".scorep_region_handle" );
        region_handle->setAlignment( 4 );

        Constant* region_description_init = ConstantStruct::get(
            region_description_type,
            ConstantExpr::getPointerCast( region_handle, i32_ptr_type ),
            ConstantExpr::getPointerCast( create_global_string( f,
                                                                demangle( f.getName().str() ),
                                                                "name" ),
                                          i8_ptr_type ),
            ConstantExpr::getPointerCast( create_global_string( f,
                                                                f.getName().str(),
                                                                "canonical_name" ),
                                          i8_ptr_type ),
            ConstantExpr::getPointerCast( create_global_string( f,
                                                                std::get<2>( metadata ),
                                                                "file" ),
                                          i8_ptr_type ),
            ConstantInt::get( i32_type,
                              std::get<0>( metadata ) ),
            ConstantInt::get( i32_type,
                              std::get<1>( metadata ) ),
            ConstantInt::get( i32_type, 0 ),
            NULL
            );

        region_description = new GlobalVariable( *m, region_description_type,
                                                 true,
                                                 GlobalValue::LinkageTypes::InternalLinkage,
                                                 region_description_init,
                                                 func_name + ".scorep_region_descr",
                                                 nullptr );
        region_description->setAlignment( 8 );
    }

    /**
     * If it is present, get the first call instruction, otherwise the last instruction of the first basic block.
     *
     * @param f
     *
     * @return
     */
    Instruction&
    get_insert_begin( Function& f )
    {
        //Get the last instruction of the first basic block or the first call instruction in it.
        auto        basic_block_iter  = f.begin();
        BasicBlock& first_basic_block = *basic_block_iter;
        for ( auto& I : first_basic_block )
        {
            if ( !isa<AllocaInst>( &I ) )
            {
                return I;
            }
        }

        auto         instruction_iter = first_basic_block.rbegin();     //++(bb.rbegin()); // second last
        Instruction& instruction      = *instruction_iter;
        return instruction;
    }

    /**
     * Get the last instruction of the last basic block. Usually this should be the return statement.
     *
     * @param f
     *
     * @return
     */
    Instruction&
    get_insert_end( Function& f )
    {
        auto         basic_block_iter = --( f.end() );
        BasicBlock&  last_basic_block = *basic_block_iter;
        auto         instruction_iter = last_basic_block.rbegin();
        Instruction& last_instruction = *instruction_iter;

        return last_instruction;
    }

    /**
     * Insert basic block for function call of Score-P's register function with if statement.
     *
     * @param insert_point
     * @param builder
     */
    void
    insert_register_region( Instruction& insert_point,
                            IRBuilder<>& builder )
    {
        builder.SetInsertPoint( &insert_point );
        Value*          load                   = builder.CreateLoad( region_handle );
        Value*          icmp                   = builder.CreateICmpEQ( load, builder.getInt32( 0 ) );
        TerminatorInst* register_plugin_branch = SplitBlockAndInsertIfThen( icmp, &insert_point, false );
        builder.SetInsertPoint( register_plugin_branch );
        Value* gep = builder.CreateInBoundsGEP( region_description, { builder.getInt32( 0 ) } );
        builder.CreateCall( scorep_plugin_register_region_func, gep );
    }

    /**
     * Insert basic block for function call of Score-P's enter function with if statement.
     *
     * @param insert_point
     * @param builder
     */
    void
    insert_enter_region( Instruction& insert_point,
                         IRBuilder<>& builder )
    {
        builder.SetInsertPoint( &insert_point );
        Value*          load                = builder.CreateLoad( region_handle );
        Value*          icmp                = builder.CreateICmpNE( load, builder.getInt32( -1 ) );
        TerminatorInst* enter_plugin_branch = SplitBlockAndInsertIfThen( icmp, &insert_point, false );
        builder.SetInsertPoint( enter_plugin_branch );
        builder.CreateCall( scorep_plugin_enter_region_func, load );
    }

    /**
     * Insert basic block for function call of Score-P's exit function with if statement.
     *
     * @param insert_point
     * @param builder
     */
    void
    insert_exit_region( Instruction& insert_point,
                        IRBuilder<>& builder )
    {
        builder.SetInsertPoint( &insert_point );
        Value*          load               = builder.CreateLoad( region_handle );
        Value*          icmp               = builder.CreateICmpNE( load, builder.getInt32( -1 ) );
        TerminatorInst* exit_plugin_branch = SplitBlockAndInsertIfThen( icmp, &insert_point, false );
        builder.SetInsertPoint( exit_plugin_branch );
        builder.CreateCall( scorep_plugin_exit_region_func, load );
    }


    /**
     * Insert exception handling to every call instruction
     * @param f
     * @param call_inst
     * @param builder
     */
    void
    expection_handling( Function& f, std::vector<CallInst*> call_inst, std::tuple<BasicBlock*, AllocaInst*, AllocaInst*> resume, IRBuilder<> builder )
    {
        // we do not need exception handling if there is no call instruction
        if ( call_inst.empty() )
        {
            return;
        }

        //exception handling
        if ( std::get<0> ( resume ) == nullptr )
        {
            resume = create_unwind_resume_block( f );
        }

        BasicBlock* resume_block = std::get<0> ( resume );

        AllocaInst* exception_ptr  = std::get<1> ( resume );
        AllocaInst* exception_type = std::get<2> ( resume );

        BasicBlock* landing_pad = create_landingpad( f, builder, resume_block, exception_ptr, exception_type );

        // change all call instructions to invoke instructions because we don't know if they could throw an exception
        // somewhere in the call stack
        for ( std::vector<CallInst*>::iterator it = call_inst.begin(), itEnd = call_inst.end(); it != itEnd; )
        {
            CallInst* call = *it++;
            changeToInvokeAndSplitBasicBlock( call, landing_pad );
        }

        //call_inst.push_back( dyn_cast<CallInst>( resume_block->getFirstNonPHI() ) );
        insert_exit_region( *resume_block->getFirstNonPHI(), builder );
        //TODO neccessary?
        if ( f.hasFnAttribute( llvm::Attribute::NoUnwind ) )
        {
            f.removeFnAttr( llvm::Attribute::NoUnwind );
        }
    }

    /**
     * Create an empty clean up landing pad
     * @param f
     * @param builder
     * @param resume_block
     * @return
     */
    BasicBlock*
    create_landingpad( Function& f, IRBuilder<>& builder, BasicBlock* resume_block, AllocaInst* exception_ptr, AllocaInst* exception_type )
    {
        BasicBlock* bb = BasicBlock::Create( f.getParent()->getContext(), "landing_pad_block", &f, resume_block );
        builder.SetInsertPoint( bb );
        LandingPadInst* pad = builder.CreateLandingPad( StructType::get( f.getContext(), { Type::getInt8PtrTy( f.getContext() ), Type::getInt32Ty( f.getContext() ) } ), 0, "finally" );
        pad->setCleanup( true );

        auto extract1 = builder.CreateExtractValue( pad, 0 );
        builder.CreateStore( extract1, exception_ptr );
        auto extract2 = builder.CreateExtractValue( pad, 1 );
        builder.CreateStore( extract2, exception_type );

        // TODO remove debugging call
        Value* load = builder.CreateLoad( region_handle );
        builder.CreateCall( scorep_plugin_dummy_func, load );

        builder.CreateBr( resume_block );


        return bb;
    }

    /**
     * Extract AllocInst from unwind resume block
     * @param bb
     * @return
     */
    std::tuple<AllocaInst*, AllocaInst*>
    get_allocaInst_from_resume( BasicBlock& bb )
    {
        std::vector<AllocaInst*> v = std::vector<AllocaInst*>();

        for ( Instruction& inst : bb )
        {
            if ( auto* load = dyn_cast<LoadInst>( &inst ) )
            {
                v.push_back( dyn_cast<AllocaInst>( load->getOperand( 0 ) ) );
            }
        }
        return std::make_tuple( v[ 0 ], v[ 1 ] );
    }

    /**
     * Create an resume block
     * @param f
     * @param caught_result_storage
     * @return
     */
    std::tuple<BasicBlock*, AllocaInst*, AllocaInst*>
    create_unwind_resume_block( Function& f )
    {
        // check should be unnecessary because if there is no resume block, there should also be no personality function
        if ( !f.hasPersonalityFn() )
        {
            f.setPersonalityFn( ConstantExpr::getPointerCast( personality_function, Type::getInt8PtrTy( f.getContext(), 0 ) ) );
        }

        AllocaInst* exception_ptr_alloca  = create_entry_block_alloca( f, Type::getInt8PtrTy( f.getContext() ) );
        AllocaInst* exception_type_alloca = create_entry_block_alloca( f, Type::getInt32Ty( f.getContext() ) );


        BasicBlock* bb = BasicBlock::Create( f.getParent()->getContext(), "/reme", &f );
        IRBuilder<> builder( bb );

        Value* exception_ptr  = builder.CreateLoad( exception_ptr_alloca );
        Value* exception_type = builder.CreateLoad( exception_type_alloca );

        Type*  lpad_type = StructType::get( f.getContext(), { Type::getInt8PtrTy( f.getContext() ), Type::getInt32Ty( f.getContext() ) } );
        Value* lpad_val  = UndefValue::get( lpad_type );
        lpad_val = builder.CreateInsertValue( lpad_val, exception_ptr, 0, "lpad.val" );
        lpad_val = builder.CreateInsertValue( lpad_val, exception_type, 1, "lpad.val" );
        builder.CreateResume( lpad_val );

        return std::make_tuple( bb, exception_ptr_alloca, exception_type_alloca );
    }


    /**
     * Create an alloc instruction into the function's begin
     * @param f
     * @param type
     * @param init_with constant initialization value
     * @param name
     * @return
     */
    AllocaInst*
    create_entry_block_alloca( Function& f, llvm::Type* type, Constant* init_with = 0, const std::string& name = "" )
    {
        BasicBlock& bb = f.getEntryBlock();
        IRBuilder<> builder( &bb, bb.begin() );

        AllocaInst* alloc = builder.CreateAlloca( type, 0, name );

        if ( init_with )
        {
            builder.CreateStore( init_with, alloc );
        }
        return alloc;
    }

    /**
     * Parse function and return all exit points and all call instructions
     * @param f
     * @return
     */
    std::tuple< std::vector<Instruction*>, std::vector<CallInst*>, std::tuple<BasicBlock*, AllocaInst*, AllocaInst*> >
    parse_function( Function& f )
    {
        std::vector<Instruction*>                         exit_points = std::vector<Instruction*>();
        std::vector<CallInst*>                            call_inst   = std::vector<CallInst*>();
        std::tuple<BasicBlock*, AllocaInst*, AllocaInst*> resume;

        for ( BasicBlock& bb : f )
        {
            for ( Instruction& inst : bb )
            {
                if ( isa<ReturnInst>( &inst ) )
                {
                    exit_points.push_back( &inst );
                }
                else if ( auto* call = dyn_cast<CallInst>( &inst ) )
                {
                    /**
                       _ZdlPv = operator delete(void*)
                       _Znwm = operator new(unsigned long)

                       // TODO filter noexcept functions
                       // TODO do not change cxa and std::exception to invoke
                     */
                    if ( call->getCalledFunction()->getName().find( "ZdlPv" ) == std::string::npos &&
                         call->getCalledFunction()->getName().find( "Znwm" )  == std::string::npos &&
                         call->getCalledFunction()->getName().find( "llvm." ) == std::string::npos
                         )
                    {
                        call_inst.push_back( call );
                    }
                }

                else if ( isa<ResumeInst> ( &inst ) )
                {
                    std::tuple<AllocaInst*, AllocaInst*> tup = get_allocaInst_from_resume( bb );
                    resume = std::make_tuple( &bb, std::get<0>( tup ), std::get<1>( tup ) );
                }
            }
        }
        return std::make_tuple( exit_points, call_inst, resume );
    }

    /**
     * Inserts basic blocks with Score-P's function calls
     *
     * @param f
     * @param builder
     */
    void
    insert_scorep_function_calls( Function& f,
                                  IRBuilder<>& builder, Instruction& start_point, std::vector<Instruction*>& exit_points    )
    {
        insert_register_region( start_point, builder );
        insert_enter_region( start_point, builder );

        for ( Instruction* inst : exit_points )
        {
            insert_exit_region( *inst, builder );
        }
    }

    /**
     * region description struct
     */
    StructType* region_description_type = nullptr;

    /** register function */
    Constant* scorep_plugin_register_region_func;

    /** enter function */
    Constant* scorep_plugin_exit_region_func;

    /** exit function */
    Constant* scorep_plugin_enter_region_func;

    /** just a dummy function for debugging TODO remove */
    Constant*       scorep_plugin_dummy_func;

    Constant*       personality_function;

    ArrayRef<Type*> exception_type;

    /******************************************************************************
     *  ATTENTION                                                                 *
     ******************************************************************************
     * all members below will be changed on every function with pass is called with
     */

    /** region handle */
    GlobalVariable* region_handle;

    /** region description  */
    GlobalVariable* region_description;
};
}

char ScorepPass::ID = 0;

// Automatically enable the pass.
// http://adriansampson.net/blog/clangpass.html
static void
registerScorepPass( const PassManagerBuilder&,
                    legacy::PassManagerBase& PM )
{
    PM.add( new ScorepPass() );
}

// add pass after all optimizations
static RegisterStandardPasses
    RegisterMyPass( PassManagerBuilder::EP_OptimizerLast,
                    registerScorepPass );
// fallback for compiling with -O0. Pass will be executed after inlining
static RegisterStandardPasses
    RegisterMyPass0( PassManagerBuilder::EP_EnabledOnOptLevel0,
                     registerScorepPass );
