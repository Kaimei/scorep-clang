/**
 * @file
 *
 * @brief  A plugin to instrument any function for usage with Score-P at compile-time.
 *
 */

#include <llvm/Pass.h>
#include <llvm/IR/Function.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/IR/LegacyPassManager.h>
#include <llvm/IR/InstrTypes.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/BasicBlock.h>
#include <llvm/Transforms/Utils/BasicBlockUtils.h>
#include <llvm/IR/Module.h>

#include <llvm/IR/DebugInfo.h>
#include <llvm/IR/DebugInfoMetadata.h>

#include <cxxabi.h>
//#include <libiberty/demangle.h>
#include <config.h>
#include <bfd.h>

//#if defined( GNU_DEMANGLE )
/* Declaration of external demangling function */
/* It is contained in "demangle.h" */
extern char*
cplus_demangle( const char* mangled,
                int         options );

/* cplus_demangle options */
#define SCOREP_COMPILER_DEMANGLE_NO_OPTS   0
#define SCOREP_COMPILER_DEMANGLE_PARAMS    ( 1 << 0 )  /* include function arguments */
#define SCOREP_COMPILER_DEMANGLE_ANSI      ( 1 << 1 )  /* include const, volatile, etc. */
#define SCOREP_COMPILER_DEMANGLE_VERBOSE   ( 1 << 3 )  /* include implementation details */
#define SCOREP_COMPILER_DEMANGLE_TYPES     ( 1 << 4 )  /* include type encodings */

/* Demangling style. */
static int scorep_compiler_demangle_style = SCOREP_COMPILER_DEMANGLE_PARAMS  |
                                            SCOREP_COMPILER_DEMANGLE_ANSI    |
                                            SCOREP_COMPILER_DEMANGLE_VERBOSE |
                                            SCOREP_COMPILER_DEMANGLE_TYPES;
//#endif /* GNU_DEMANGLE */

using namespace llvm;

namespace
{
struct ScorepPass : public FunctionPass
{
    static char ID;

    ScorepPass() : FunctionPass( ID )
    {
    }

    virtual bool
    runOnFunction( Function& F )
    {
        errs() << "I saw a function called " << F.getName() << "!\n";

        if ( !is_instrumentable( F ) )
        {
            return false;
        }


        Module*      m   = F.getParent();
        LLVMContext& ctx = F.getContext();
        IRBuilder<>  builder( ctx );


        if ( region_descriptor == nullptr )
        {
            create_struct_type( m );
            create_function_prototypes( m );
        }

        create_global_vars( F, builder );

        handle_basic_block( F, builder );

        return true;
    }

private:

    /**
     * Check if a function can be instrumented
     * @param f function to check
     * @return true, if function can be instrumented
     */
    bool
    is_instrumentable( const Function& f )
    {
        std::string error;
        if ( has_no_instrument_attribute( f, &error )
             || has_empty_body( f, &error )
             || is_openmp_function( f, &error )
             || is_artificial( f, &error )
             || is_scorep_filtered( f, &error ) )
        {
            errs() << "Ignore: " + f.getName().str() + " reason: " + error + "\n";
            return false;
        }
        return true;
    }

    /**
     * Check if function has an empty body
     * @param f function to check
     * @param error pointer to error message
     * @return true, if function has an empty body
     * @todo use built-in functions?
     */
    bool
    has_empty_body( const Function& f, std::string* error = nullptr )
    {
        unsigned long size = f.getEntryBlock().getInstList().size();

        if ( f.getReturnType() == Type::getVoidTy( f.getContext() ) && size == 1 )
        {
            if ( error != nullptr )
            {
                *error = "is empty";
            }
            return true;
        }
        return false;
    }

    /** Check if functions name starts with '.omp_outlined.
     * @param f function to check
     * @param error pointer to error message
     * @return true, if functions name starts with '.omp_outlined.'
     */
    bool
    is_openmp_function( const Function& f, std::string* error = nullptr )
    {
        std::string omp_outlined_func_name( ".omp_outlined." );
        std::string func_name_as_string = f.getName().str();
        if ( !func_name_as_string.compare( 0, omp_outlined_func_name.size(), omp_outlined_func_name ) )
        {
            if ( error != nullptr )
            {
                *error = "is OMP outlined";
            }
            return true;
        }
        return false;
    }

    /**
     * Check if function has attribute "scorep_no_instrument"
     * @param f function to check
     * @param error pointer to error message
     * @return true, if it has attribute "scorep_no_instrument"
     * @todo is ignored?
     */
    bool
    has_no_instrument_attribute( const Function& f, std::string* error = nullptr )
    {
        if ( f.hasFnAttribute( "scorep_no_instrument" ) )
        {
            if ( error != nullptr )
            {
                *error = "has attribute scorep_no_instrument";
            }
            return true;
        }
        return false;
    }

    /**
     * Check if the function is created artificially by the compiler
     * @param f function to check
     * @param error pointer to error message
     * @return true, if function is artificial
     * @todo better characteristic
     */
    bool
    is_artificial( const Function& f, std::string* error = nullptr )
    {
        std::string cxx_name            = "__cxx_";
        std::string glob_name           = "_GLOBAL__";
        std::string func_name_as_string = f.getName().str();

        if ( func_name_as_string.find( cxx_name ) != std::string::npos
             || func_name_as_string.find( glob_name ) != std::string::npos )
        {
            if ( error != nullptr )
            {
                *error = "is artificial";
            }
            return true;
        }
        return false;
    }

    /**
     * Check if the function is filtered by ScoreP file
     * @param f function to check
     * @param error pointer to error message
     * @return true, if function is filtered
     * @todo implementation
     */
    bool
    is_scorep_filtered( const Function& f, std::string* error = nullptr )
    {
        return false;
    }

    /**
     * Create the struct type for region_descriptor and insert it to LLVM IR.
     *
     * @param m     module in which context struct will be inserted
     *
     * @warning do not change order and insert new elements only ar the end
     */
    void
    create_struct_type( Module* m )
    {
        LLVMContext& ctx = m->getContext();

        auto* i32_type     = Type::getInt32Ty( ctx );
        auto* i32_ptr_type = PointerType::get( i32_type, 0 );
        //auto *i8_ptr_type = PointerType::get(Type::getInt8PtrTy(ctx), 0);
        auto* i8_ptr_type = PointerType::getInt8PtrTy( ctx );


        region_descriptor = StructType::create( ctx, "struct.scorep_region_description" );
        std::vector<llvm::Type*> params;
        params.push_back( i32_ptr_type );               // handle ref
        params.push_back( i8_ptr_type );                // name
        params.push_back( i8_ptr_type );                // canonical name
        params.push_back( i8_ptr_type );                // file
        params.push_back( i32_type );                   // begin lno
        params.push_back( i32_type );                   // end lno
        params.push_back( i32_type );                   // flags

        region_descriptor->setBody( params );
    }

    /**
     * Get pretty name from canonical.
     *
     * @param s     Mangled region name.
     *
     * @return
     *
     * @todo implement
     */
/*    std::string
    demangle( const std::string& str )
    {
        char* demangled = abi::__cxa_demangle( str.c_str(), NULL, NULL, NULL );

        std::string res = str;
        if ( demangled != nullptr )
        {
            res = std::string( demangled );
        }
        else
        {
            res = str;
        }

        ::free( demangled );
        return res;
    }*/

    /**
     * Get pretty name from canonical.
     *
     * @param s     Mangled region name.
     *
     * @return
     *
     * @todo implement
     */
    std::string
    demangle( const std::string& funcname )
    {
        const char* funcname_demangled = funcname.c_str();
        //#if defined( GNU_DEMANGLE )
        /* use demangled name if possible */
        if ( scorep_compiler_demangle_style >= 0 )
        {
            funcname_demangled = cplus_demangle( funcname.c_str(),
                                                 scorep_compiler_demangle_style );

            if ( funcname_demangled == NULL )
            {
                funcname_demangled = funcname.c_str();
            }
        }
        //#endif
        return std::string( funcname_demangled );
    }

    /**
     * Create prototypes for Score-P's register, enter and exit function and insert them to LLVM IR.
     *
     * @param m     module in which context prototypes will be inserted
     */
    void
    create_function_prototypes( Module* m )
    {
        LLVMContext& ctx       = m->getContext();
        Type*        void_type = Type::getVoidTy( ctx );
        Type*        i32_type  = Type::getInt32Ty( ctx );

        auto* string_ptr_type = PointerType::get( region_descriptor, 0 );

        /** register linked Score-P functions */
        scorep_plugin_register_region_func = m->getOrInsertFunction( "scorep_plugin_register_region", void_type,
                                                                     string_ptr_type, NULL );
        scorep_plugin_exit_region_func = m->getOrInsertFunction( "scorep_plugin_exit_region", void_type, i32_type,
                                                                 NULL );
        scorep_plugin_enter_region_func = m->getOrInsertFunction( "scorep_plugin_enter_region", void_type, i32_type,
                                                                  NULL );
    }

    /**
     * Create static strings for descriptor.
     *
     * @param f
     * @param builder
     */
    void
    create_strings( Function&   f,
                    IRBuilder<> builder )
    {
        create_string_name( f, builder );
        create_string_canonical_name( f, builder );
        create_string_file( f, builder );
    }

    /**
     * Create string for function name and add it to the descriptor.
     *
     * @param f
     * @param builder
     *
     * @todo get name from canonical
     */
    void
    create_string_name( Function&   f,
                        IRBuilder<> builder )
    {
        auto  name_text = ConstantDataArray::getString( f.getContext(), demangle( f.getName().str() ) );
        auto* name      = new GlobalVariable( *f.getParent(), name_text->getType(), true,
                                              GlobalValue::LinkageTypes::PrivateLinkage, name_text,
                                              f.getName() + ".name" );
        name->setAlignment( 1 );

        Value* gep = builder.CreateInBoundsGEP( region_description, { builder.getInt32( 0 ), builder.getInt32( 1 ) } );
        builder.CreateStore( ConstantExpr::getBitCast( name, builder.getInt8PtrTy( 0 ) ), gep );
    }

    /**
     * Create string for function's canonical name and add it to the descriptor.
     *
     * @param f
     * @param builder
     */
    void
    create_string_canonical_name( Function&   f,
                                  IRBuilder<> builder )
    {
        auto  canonical_name_text = ConstantDataArray::getString( f.getContext(), f.getName() );
        auto* canonical_name      = new GlobalVariable( *f.getParent(), canonical_name_text->getType(), true,
                                                        GlobalValue::LinkageTypes::PrivateLinkage, canonical_name_text,
                                                        f.getName() + ".canonical_name" );
        canonical_name->setAlignment( 1 );

        Value* gep = builder.CreateInBoundsGEP( region_description, { builder.getInt32( 0 ), builder.getInt32( 2 ) } );
        builder.CreateStore( ConstantExpr::getBitCast( canonical_name, builder.getInt8PtrTy( 0 ) ), gep );
    }

    /**
     * Create string for the file where the function is defined and add it to the descriptor.
     *
     * @param f
     * @param builder
     */
    void
    create_string_file( Function&   f,
                        IRBuilder<> builder )
    {
        auto  file_text = ConstantDataArray::getString( f.getContext(), f.getParent()->getSourceFileName() );
        auto* file      = new GlobalVariable( *f.getParent(), file_text->getType(), true,
                                              GlobalValue::LinkageTypes::PrivateLinkage, file_text,
                                              f.getName() + ".file" );
        file->setAlignment( 1 );

        Value* gep = builder.CreateInBoundsGEP( region_description, { builder.getInt32( 0 ), builder.getInt32( 3 ) } );
        builder.CreateStore( ConstantExpr::getBitCast( file, builder.getInt8PtrTy( 0 ) ), gep );
    }

    /**
     * Creates handler for region and descriptor and add them to LLVM IR.
     *
     * @param f
     * @param builder
     */
    void
    create_global_vars( Function&   f,
                        IRBuilder<> builder )
    {
        Module*   m               = f.getParent();
        StringRef func_name       = f.getName();
        auto*     string_ptr_type = PointerType::get( region_descriptor, 0 );

        region_handle = new GlobalVariable( *m, Type::getInt32Ty( m->getContext() ), false,
                                            GlobalValue::LinkageTypes::InternalLinkage,
                                            builder.getInt32( 0 ),
                                            func_name + ".scorep_region_handle" );
        region_handle->setAlignment( 4 );

        region_description = new GlobalVariable( *m, region_descriptor, false,
                                                 GlobalValue::LinkageTypes::InternalLinkage,
                                                 ConstantPointerNull::get( string_ptr_type ),
                                                 func_name + ".scorep_region_descr", nullptr );
//        region_description = new GlobalVariable( *m, string_ptr_type, false,
//                                                  GlobalValue::LinkageTypes::InternalLinkage,
//                                                  nullptr,
//                                                  func_name + ".scorep_region_descr", nullptr );
        region_description->setAlignment( 8 );
    }

    /**
     * If it is present, get the first call instruction, otherwise the last instruction of the first basic block.
     *
     * @param f
     *
     * @return
     */
    Instruction&
    get_insert_begin( Function& f )
    {
        //Get the last instruction of the first basic block or the first call instruction in it.
        auto        basic_block_iter  = f.begin();
        BasicBlock& first_basic_block = *basic_block_iter;
        for ( auto& I : first_basic_block )
        {
            if ( auto* op = dyn_cast<CallInst>( &I ) )
            {
                return I;
            }
        }

        auto         instruction_iter = first_basic_block.rbegin();     //++(bb.rbegin()); // second last
        Instruction& instruction      = *instruction_iter;
        return instruction;
    }

    /**
     * Get the last instruction of the last basic block. Usually this should be the return statement.
     *
     * @param f
     *
     * @return
     */
    Instruction&
    get_insert_end( Function& f )
    {
        auto         basic_block_iter = --( f.end() );
        BasicBlock&  last_basic_block = *basic_block_iter;
        auto         instruction_iter = last_basic_block.rbegin();
        Instruction& last_instruction = *instruction_iter;

        // insert Score-P exit region
        return last_instruction;
    }

    /**
     * Add Score-P's handler to Score-P's descriptor.
     *
     * @param f
     * @param builder
     */
    void
    append_handle_to_struct( const Function& f,
                             IRBuilder<>&    builder )
    {
        Value* gep = builder.CreateGEP( region_description, { builder.getInt32( 0 ), builder.getInt32( 0 ) } );
        builder.CreateStore( ConstantExpr::getBitCast( region_handle, Type::getInt32PtrTy( f.getContext() ) ),
                             gep );
    }

    /**
     * Insert function call of Score-P's register function with if statement.
     *
     * @param inst
     * @param builder
     */
    void
    insert_register_region( Instruction& inst,
                            IRBuilder<>& builder )
    {
        // insert Score-P register region
        Value*          load                   = builder.CreateLoad( region_handle );
        Value*          icmp                   = builder.CreateICmpEQ( load, builder.getInt32( 0 ) );
        TerminatorInst* register_plugin_branch = SplitBlockAndInsertIfThen( icmp, &inst, false );
        builder.SetInsertPoint( register_plugin_branch );
        Value* gep = builder.CreateInBoundsGEP( region_description, { builder.getInt32( 0 ) } );
        builder.CreateCall( scorep_plugin_register_region_func, gep );
    }

    /**
     * Insert function call of Score-P's enter function with if statement.
     *
     * @param inst
     * @param builder
     */
    void
    insert_enter_region( Instruction& inst,
                         IRBuilder<>& builder )
    {
        Value*          load                = builder.CreateLoad( region_handle );
        Value*          icmp                = builder.CreateICmpNE( load, builder.getInt32( -1 ) );
        TerminatorInst* enter_plugin_branch = SplitBlockAndInsertIfThen( icmp, &inst, false );
        builder.SetInsertPoint( enter_plugin_branch );
        builder.CreateCall( scorep_plugin_enter_region_func, load );
    }

    /**
     * Insert function call of Score-P's exit function with if statement.
     *
     * @param inst
     * @param builder
     */
    void
    insert_exit_region( Instruction& inst,
                        IRBuilder<>& builder )
    {
        Value*          load               = builder.CreateLoad( region_handle );
        Value*          icmp               = builder.CreateICmpNE( load, builder.getInt32( -1 ) );
        TerminatorInst* exit_plugin_branch = SplitBlockAndInsertIfThen( icmp, &inst, false );
        builder.SetInsertPoint( exit_plugin_branch );
        builder.CreateCall( scorep_plugin_exit_region_func, load );
    }

    /**
     * Inserts basic blocks with Score-P's function calls
     *
     * @param f
     * @param builder
     */
    void
    handle_basic_block( Function&    f,
                        IRBuilder<>& builder )
    {
        Instruction& begin = get_insert_begin( f );
        builder.SetInsertPoint( &begin );
        append_handle_to_struct( f, builder );
        create_strings( f, builder );

        builder.SetInsertPoint( &begin );
        insert_register_region( begin, builder );

        builder.SetInsertPoint( &begin );
        insert_enter_region( begin, builder );

        Instruction& last = get_insert_end( f );
        builder.SetInsertPoint( &last );
        insert_exit_region( last, builder );
    }

    /**
     * region description struct
     */
    StructType* region_descriptor = nullptr;

    /******************************************************************************
     *  ATTENTION                                                                 *
     ******************************************************************************
     * all members above will be changed on every function with pass is called with
     */

    /** register function */
    Constant* scorep_plugin_register_region_func;

    /** enter function */
    Constant* scorep_plugin_exit_region_func;

    /** exit function */
    Constant* scorep_plugin_enter_region_func;

    /** region handle */
    GlobalVariable* region_handle;

    /** region description  */
    GlobalVariable* region_description;
};
}

char ScorepPass::ID = 0;

// Automatically enable the pass.
// http://adriansampson.net/blog/clangpass.html
static void
registerScorepPass( const PassManagerBuilder&,
                    legacy::PassManagerBase& PM )
{
    PM.add( new ScorepPass() );
}

// add pass after all optimizations
static RegisterStandardPasses
    RegisterMyPass( PassManagerBuilder::EP_OptimizerLast,
                    registerScorepPass );
// fallback for compiling with -O0. Pass will be executed after inlining
static RegisterStandardPasses
    RegisterMyPass0( PassManagerBuilder::EP_EnabledOnOptLevel0,
                         registerScorepPass );
